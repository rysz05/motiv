const express = require('express');
const bodyParser = require('body-parser')
require('dotenv').config();
const routes = require('./routes/api');
const path = require('path');

const app = express();
const port = process.env.PORT || 3002;

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next(); });

app.use(bodyParser.json())
//app.use(express.static('./frontend/src'))

app.use((err, req, res, next) => {
 console.log(err);
 next()
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`)
});
