import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch,
  useRouteMatch
} from "react-router-dom";
import "./cards.css";

import {hideComponent} from './methods.js'

import Cooking from "./categories/cooking.js"
import Craft from "./categories/craft.js"
import Cows from "./categories/cows.js"
import Garden from "./categories/garden.js"
import Holidays from "./categories/holidays.js"
import Party from "./categories/party.js"
import Reading from "./categories/reading.js"
import Study from "./categories/study.js"

function Categories() {

    let { path, url } = useRouteMatch();

    let categories = [
      {name: "craft", component: "Craft", address: "https://cdn.openpr.com/T/b/Tb27926546_g.jpg"},
      {name: "garden", component: "Garden", address: "https://www.gannett-cdn.com/media/2020/03/12/USATODAY/usatsports/gettyimages-1137926593.jpg?auto=webp&crop=2148,1208,x0,y94&format=pjpg&width=1200"},
      {name: "cooking", component: "Cooking", address: "https://bushaircargo.com/wp-content/uploads/2021/02/cooking.jpg"},
      {name: "study", component: "Study", address: "https://tutorcruncher.com/assets/blog/exam-tutoring-15f8d04.c010259.jpg"},
      {name: "reading", component: "Reading", address: "https://learnenglish.britishcouncil.org/sites/podcasts/files/styles/article/public/RS7900_ThinkstockPhotos-899906850-hig.jpg?itok=QzJ0KH-k"},
      {name: "holidays", component: "Holidays", address: "https://images.financialexpress.com/2020/12/airlines-1.jpg"},
      {name: "party", component: "Party", address: "https://opycha.pl/wp-content/uploads/2019/04/ben-rosett-10611-unsplash-min.jpg"},
      {name: "cows", component: "Cows", address: "https://cdn.pixabay.com/photo/2020/10/09/14/11/cows-5640641_960_720.jpg"}
    ]

    return (
      <Router>
      <div>
        <section className="hero-section">
          <div className="card-grid">
            {categories.map(cat => (
               <Link to={`${url}/${cat.name}`} className="card" key={`${cat.name}`} onClick={hideComponent}>
                <div className="card__background" style={{backgroundImage: `url(${cat.address}`}}></div>
                <div className="card__content">
                  <p className="card__category">Category</p>
                  <h3 className="card__heading">{cat.name}</h3>
                </div>
              </Link>
              ))}
          </div>
        </section>
      </div>
      <div className="content">
        <Switch>
          <Route path= {`${path}/cooking`} component={Cooking} />
          <Route path={`${path}/craft`} component={Craft} />
          <Route path={`${path}/garden`} component={Garden} />
          <Route path={`${path}/study`} component={Study} />
          <Route path={`${path}/reading`} component={Reading} />
          <Route path={`${path}/holidays`} component={Holidays} />
          <Route path={`${path}/party`} component={Party} />
          <Route path={`${path}/cows`} component={Cows} />
        </Switch>
      </div>
      </Router>
    );
}


export default Categories;


