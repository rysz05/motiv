import React from 'react';
import PropTypes from 'prop-types';
import "../cards.css";

export default function Task_wrapper({ name, key }) {
	return (
		<div className="task-wrap" draggable>
		  <div className="box">
		    <input id={name} type="checkbox" />
		    <span className="check"></span>
		    <label for={name}>{name}</label>
		  </div>
		</div>
	)
}

Task_wrapper.propTypes = {
	name: PropTypes.arrayOf(PropTypes.string).isRequired
}
