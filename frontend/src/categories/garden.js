import React from "react";
import Task_wrapper from "./task-wrapper.js"


let tasks = [{name: "buy soil" }, {name: "buy tools"}, {name: "fertillize"}, {name: "weed weeds"}, {name: "water"},{name: "move inside"}, {name:"cut branches"}]

export default function Garden() {
    return (
      <div className="wrapper">
      	{tasks.map(task =>
      		<Task_wrapper
      			name={task.name}
      			key={task.name}
      		/>
      	)}
      </div>
    );
}

