import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Link,
  Route,
} from "react-router-dom";
import Categories from "./categories.js"

import ReactDOM from 'react-dom';
import './reset.css';
import './index.css';
import hideButton from './methods.js'

function Landing() {


    return (
      <div>
        <Link to="/categories" className="buttonBox">
          <button className="learn-more" onClick={hideButton}>Add Task</button>
        </Link>
          <Route path="/categories">
            <Categories />
          </Route>
      </div>
    )
}

// ========================================

ReactDOM.render(
  <Router>
    <Landing />
  </Router>,
  document.getElementById('root')
);

